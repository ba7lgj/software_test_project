package org.ba7lgj.software_test_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.stereotype.Component;

@SpringBootApplication

public class SoftwareTestProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoftwareTestProjectApplication.class, args);
    }

}
