package org.ba7lgj.software_test_project.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class R {
    private int code;
    private String msg;
    private Object data;


    public static R ok(Object data){
        return new R(200,"操作成功",data);
    }
    public static R err(int code,String msg){
        return new R(code,msg,null);
    }

}
