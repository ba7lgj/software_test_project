package org.ba7lgj.software_test_project.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author BA7LGJ
 * @since 2024-05-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("academy")
public class Academy extends Base {

    private static final long serialVersionUID = 1L;

    @TableId(value = "academy_id", type = IdType.AUTO)
    private Integer academyId;

    @TableField("academy_name")
    private String academyName;

}
