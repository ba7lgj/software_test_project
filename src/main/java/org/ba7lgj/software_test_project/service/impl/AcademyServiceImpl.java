package org.ba7lgj.software_test_project.service.impl;

import org.ba7lgj.software_test_project.bean.Academy;
import org.ba7lgj.software_test_project.mapper.AcademyMapper;
import org.ba7lgj.software_test_project.service.IAcademyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author BA7LGJ
 * @since 2024-05-28
 */
@Service
public class AcademyServiceImpl extends ServiceImpl<AcademyMapper, Academy> implements IAcademyService {

}
