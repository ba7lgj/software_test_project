package org.ba7lgj.software_test_project.service;

import org.ba7lgj.software_test_project.bean.Academy;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author BA7LGJ
 * @since 2024-05-28
 */
public interface IAcademyService extends IService<Academy> {

}
