package org.ba7lgj.software_test_project.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.ba7lgj.software_test_project.bean.Academy;
import org.ba7lgj.software_test_project.bean.R;
import org.ba7lgj.software_test_project.service.IAcademyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/academy")
@CrossOrigin(origins = "*")
public class AcademyController {


    @Autowired
    private IAcademyService academyService;

    @PostMapping("/add")
    public R addAcademy(@Valid @RequestBody Academy academy){
        boolean save = academyService.save(academy);
        return save?R.ok(null):R.err(400,"操作失败");
    }


    @PostMapping("/del")
    public R delAcademy(@Valid @RequestBody Academy academy){
        boolean b = academyService.removeById(academy);
        return b?R.ok(null):R.err(400,"操作失败");
    }

    @PostMapping("/modify")
    public R modifyAcademy(@Valid @RequestBody Academy academy){
        boolean b = academyService.updateById(academy);
        return b?R.ok(null):R.err(400,"操作失败");
    }

    @GetMapping("/list")
    public R academyList(@RequestParam Integer current,@RequestParam Integer size){

        Page<Academy> list = academyService.page(        academyService.page(new Page<>(current,size)));
        R ok = R.ok(list);
        return R.ok(list);
    }

}
