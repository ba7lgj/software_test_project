package org.ba7lgj.software_test_project.handler;
import org.ba7lgj.software_test_project.bean.R;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<R> handleValidationExceptions(Exception ex) {
        ex.printStackTrace();
        return ResponseEntity.badRequest().body(R.err(400,"肯定是你的问题,反正不是服务器问题"));
    }
}


