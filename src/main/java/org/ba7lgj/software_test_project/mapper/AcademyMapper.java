package org.ba7lgj.software_test_project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.ba7lgj.software_test_project.bean.Academy;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author BA7LGJ
 * @since 2024-05-28
 */
@Mapper
public interface AcademyMapper extends BaseMapper<Academy> {

}
